# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.3](https://github.com/AmazeeLabs/silverback-mono/compare/@-drupal/cypress@1.0.2...@-drupal/cypress@1.0.3) (2020-09-15)

**Note:** Version bump only for package @-drupal/cypress





## [1.0.2](https://github.com/AmazeeLabs/silverback-mono/compare/@-drupal/cypress@1.0.1...@-drupal/cypress@1.0.2) (2020-09-15)

**Note:** Version bump only for package @-drupal/cypress





## 1.0.1 (2020-09-15)


### Bug Fixes

* **drupal/cypress:** auto-update dependencies ([d57ac11](https://github.com/AmazeeLabs/silverback-mono/commit/d57ac11a0b0dabb57c88ca50dc4c8fc4cf6fda26))
* **drupal/cypress:** fixed Drupal 9 compatibility ([58ef26e](https://github.com/AmazeeLabs/silverback-mono/commit/58ef26e4c3db05f49485720beb2046970a04f961))
* **silverback-cli:** ensure .env exists before starting processes ([37fcf19](https://github.com/AmazeeLabs/silverback-mono/commit/37fcf19c8c3acb97d2247a739aa5469fb79e5e5f))
